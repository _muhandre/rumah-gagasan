# Rumah Gagasan

Link website : rumah-gagasan.herokuapp.com/

## Cara instalasi di lokal
> Catatan: setiap command 'python' di bawah ini belum tentu dapat dijalankan pada komputer anda. Maka dari itu, sesuaikan dengan *executable* `python` yang ada di perangkat anda.

1.  Pastikan git dan python telah ter-*install* pada perangkat anda. Apabila belum, anda dapat melakukan instalasi dengan mengunduh melalui link berikut: <br>
[Download Python](https://www.python.org/downloads/)<br>
[Download git](https://git-scm.com/downloads)

2. Cek apakah python dan git telah ter-install dengan baik dan benar dengan menjalankan *command* berikut pada command prompt/terminal anda.
```shell
   pip --version
   git --version
``` 

3. Clone repository ini ke suatu direktori yang kamu persiapkan dengan menjalankan *command* berikut.
```shell
   git clone https://gitlab.com/_muhandre/rumah-gagasan.git
``` 

4. Masuk ke direktori yang di hasilkan dari eksekusi langkah sebelumnya.
```shell
   cd rumah-gagasan
``` 

5. Instalasi semua *packages* yang ada pada requirements.txt kepada perangkat anda, dengan eksekusi *command* berikut.
```shell
   python -m pip install -r requirements.txt
``` 
> Catatan: Instalasi package pada langkah ini akan ter-*install* langsung pada komputer anda secara global. Apabila anda tidak ingin seperti itu, anda dapat instalasi ke sebuah *virtual environtment*. Langkah-langkah pembuatan *virtual environtment* dapat anda lihat pada *section* di bawah.

6. Buat berkas-berkas *migrations* yang akan diproses lebih lanjut dalam pembuatan database lokal.
```shell
   python manage.py makemigrations
``` 

7. Buat database lokal dengan menjalankan *command* berikut:
```shell
   python manage.py migrate
``` 

8. Kumpulkan berkas *static* yang ada pada repository ini dengan *command* berikut:
```shell
   python manage.py collectstatic
``` 

9. Langkah terakhir, anda dapat menjalankan *web server* ini secara lokal dengan perintah berikut:
```shell
   python manage.py runserver
``` 

## Pembuatan virtual environtment
Apabila anda hendak membuat *virtual environtment*, maka anda lakukan langkah-langkah berikut sebelum menjalankan langkah ke-5 pada "Cara instalasi di lokal"

1. Buat *virtual environtment* python dengan mengeksekusi:
```shell
   python -m venv venv
```

2. Setelah anda buat *virtual environtment* nya, anda dapat mengaktifkannya dengan:

Pada Windows:
```shell
   venv\Scripts\activate
```

Pada Linux/macOS:
```shell
   source venv/bin/activate
```
Akan muncul (venv) pada prompt cmd/terminal kamu yang menunjukkan *virtual environtment* sudah aktif.