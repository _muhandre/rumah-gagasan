from django.urls import path
from .views import index, myBlog, createBlog, blogDetail, UpdateBlog, deleteBlog, createComment, likeBlog

app_name = 'blogs'

urlpatterns = [
    path('', index, name='blogs-index'),
    path('my-blog', myBlog, name='blogs-myBlog'),
    path('create-blog', createBlog, name='blogs-createBlog'),
    path('blog/<int:id>', blogDetail, name='blogs-blogDetail'),
    path('blog/update/<int:pk>', UpdateBlog.as_view(), name='blogs-updateBlog'),
    path('blog/delete/<int:id>', deleteBlog, name='blogs-deleteBlog'),
    path('blog/create-comment/<int:id>', createComment, name='blogs-createComment'),
    path('blog/like/<int:id>', likeBlog, name='blogs-likeBlog')
]