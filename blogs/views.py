from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect
from .models import Blog, Comment
from .forms import BlogForm, CommentForm
from django.contrib.auth.decorators import login_required
from django.views.generic import UpdateView
from django.utils.decorators import method_decorator

# Create your views here.
def index(request):
    context = {
        'blogs' : Blog.objects.all()
    }
    return render(request,"blogs/index.html", context)

@login_required(login_url='/authentication/login')
def myBlog(request):
    my_blogs = []
    for blog in Blog.objects.all():
        if request.user == blog.author:
            my_blogs.append(blog)
    context = {
        'my_blogs' : my_blogs
    }
    return render(request, "blogs/myBlog.html", context)

@login_required(login_url='/authentication/login')
def createBlog(request):
    import base64

    if request.method == "POST":
        blog_form = BlogForm(request.POST, request.FILES)
        if blog_form.is_valid():
            blog = blog_form.save(commit = False)
            blog.author = request.user
            with blog_form.cleaned_data["thumbnail"].open('rb') as file:
                blog.base64_thumbnail = "data:image/png;base64,{}".format(base64.b64encode(file.read()).decode())
                blog.save()
            return HttpResponseRedirect('/')
    
    context = {
        'blog_form' : BlogForm()
    }
    
    return render(request, 'blogs/createBlog.html', context)

def blogDetail(request, id):
    blog = get_object_or_404(Blog, pk = id)
    context = {
        'blog' : blog,
        'isAuthor' : (blog.author == request.user),
        'allComment' : Comment.objects.filter(blog = blog),
        'commentForm' : CommentForm()
    }
    return render(request, 'blogs/blogDetail.html', context)

@login_required(login_url='/authentication/login')
def deleteBlog(request, id):
    get_object_or_404(Blog, pk = id).delete()
    if request.path == '/myblogs/':
        return redirect('blogs:blogs-myBlogs')
    return redirect('blogs:blogs-index')

def createComment(request, id):
    if request.method == "POST":
        comment_data = CommentForm(request.POST or None)
        if comment_data.is_valid():
            blog = get_object_or_404(Blog, pk = id)
            comment_text = request.POST['comment']
            if(request.user): 
                comment = Comment.objects.create(blog = blog, comment = comment_text)
            else:
                comment = Comment.objects.create(blog = blog, comment = comment_text)
            comment.save()
    redirect_url = "/blog/" + str(id)
    return HttpResponseRedirect(redirect_url)

def likeBlog(request, id):
    blog = get_object_or_404(Blog, pk = id)
    if blog.likes.filter(id = request.user.id).exists():
        blog.likes.remove(request.user)
    else:
        blog.likes.add(request.user)
    return redirect('blogs:blogs-index')

@method_decorator(login_required, name='dispatch')
class UpdateBlog(UpdateView):
    model = Blog
    form_class = BlogForm
    template_name = 'blogs/updateBlog.html'



