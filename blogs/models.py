from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse

# Create your models here.
class Blog(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    thumbnail = models.ImageField(upload_to='images', default = 'static/Blogs/images/no-image.jpg')
    base64_thumbnail = models.TextField(null=True)
    post_date = models.DateField(auto_now_add = True)
    title = models.CharField(max_length = 200, blank = False)
    body = models.TextField(max_length = 10000)
    likes = models.ManyToManyField(User, related_name='blog_for_like')
    
    def total_likes(self):
        return self.likes.count()

    def get_absolute_url(self):
        return reverse('blogs:blogs-index')


class Comment(models.Model):
    blog = models.ForeignKey(Blog, on_delete=models.CASCADE)
    comment = models.TextField()
    post_date = models.DateField(auto_now_add = True)