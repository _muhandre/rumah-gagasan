from django.urls import path
from .views import auth_signup, auth_login, auth_logout

app_name = 'authentication'

urlpatterns = [
    path('signup/', auth_signup, name = 'authentication-signup'),
    path('login/', auth_login, name='authentication-login'),
    path('logout/', auth_logout, name='authentication-logout'),
]