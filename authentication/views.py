from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from .forms import SignupForm

def auth_signup(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get("username")
            messages.success(request, 'Akun dengan username ' + username + " telah berhasil dibuat")
        else:
            messages.error(request, "Data input tidak valid")
        return redirect('authentication:authentication-signup')
    else:
        context = {'form' : SignupForm()}
        return render(request, "authentication/signupPage.html", context)

def auth_login(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username = username, password = password)
        if user is not None:
            login(request, user)
            request.session['user_logged_username'] = username
            return redirect('blogs:blogs-index')
        else:
            messages.error(request, "Username atau password salah")
            return redirect('authentication:authentication-login')
    else:
        return render(request, "authentication/loginPage.html", context = {})

def auth_logout(request):
    logout(request)
    return redirect('blogs:blogs-index')
